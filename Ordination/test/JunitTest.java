package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalTime;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

import org.junit.Test;

import service.Service;
import storage.Storage;

public class JunitTest {

//	@BeforeClass
//	public static void setupBefore() {
//		Service.createSomeObjects();
//	}

	@Test
	public void TC1() {
		LocalDate inputStart = LocalDate.of(2015, 9, 10);
		LocalDate inputSlut = LocalDate.of(2015, 10, 10);
		Patient p1 = new Patient("180793-1010", "Jhon", 20);
		DagligFast df1 = new DagligFast(inputStart, inputSlut);
		Laegemiddel lm1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15,
				0.16, "Styk");
		p1.setOrdination(df1);
		df1.setLaegemiddel(lm1);

		assertEquals(2, Service.anbefaletDosisPrDoegn(p1, lm1),0.01);	
	}

	@Test
	public void TC2() {
		LocalDate inputStart = LocalDate.of(2015, 9, 10);
		LocalDate inputSlut = LocalDate.of(2015, 10, 10);
		Patient p1 = new Patient("180793-1010", "Jhon", 70);
		DagligFast df1 = new DagligFast(inputStart, inputSlut);
		Laegemiddel lm1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15,
				0.16, "Styk");
		p1.setOrdination(df1);
		df1.setLaegemiddel(lm1);

		assertEquals(10.5, Service.anbefaletDosisPrDoegn(p1, lm1),0.01);	
	}

	@Test
	public void TC3() {
		LocalDate inputStart = LocalDate.of(2015, 9, 10);
		LocalDate inputSlut = LocalDate.of(2015, 10, 10);
		Patient p1 = new Patient("180793-1010", "Jhon", 140);
		DagligFast df1 = new DagligFast(inputStart, inputSlut);
		Laegemiddel lm1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15,
				0.16, "Styk");
		p1.setOrdination(df1);
		df1.setLaegemiddel(lm1);

		assertEquals(22.4, Service.anbefaletDosisPrDoegn(p1, lm1),0.01);	
	}

	@Test
	public void TC4() {
		LocalDate inputStart = LocalDate.of(2015, 9, 10);
		LocalDate inputSlut = LocalDate.of(2015, 10, 10);
		Patient p1 = new Patient("180793-1010", "Jhon", 20);
		DagligFast df1 = new DagligFast(inputStart, inputSlut);
		Laegemiddel lm1 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		p1.setOrdination(df1);
		df1.setLaegemiddel(lm1);

		assertEquals(20, Service.anbefaletDosisPrDoegn(p1, lm1),0.01);	
	}

	@Test
	public void TC5() {
		LocalDate inputStart = LocalDate.of(2015, 9, 10);
		LocalDate inputSlut = LocalDate.of(2015, 10, 10);
		Patient p1 = new Patient("180793-1010", "Jhon", 70);
		DagligFast df1 = new DagligFast(inputStart, inputSlut);
		Laegemiddel lm1 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		p1.setOrdination(df1);
		df1.setLaegemiddel(lm1);

		assertEquals(105, Service.anbefaletDosisPrDoegn(p1, lm1),0.01);	
	}

	@Test
	public void TC6() {
		LocalDate inputStart = LocalDate.of(2015, 9, 10);
		LocalDate inputSlut = LocalDate.of(2015, 10, 10);
		Patient p1 = new Patient("180793-1010", "Jhon", 140);
		DagligFast df1 = new DagligFast(inputStart, inputSlut);
		Laegemiddel lm1 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		p1.setOrdination(df1);
		df1.setLaegemiddel(lm1);

		assertEquals(280, Service.anbefaletDosisPrDoegn(p1, lm1),0.01);	
	}

	@Test
	public void TC7() {
		LocalDate inputStart = LocalDate.of(2015, 9, 10);
		LocalDate inputSlut = LocalDate.of(2015, 10, 10);
		Patient p1 = new Patient("180793-1010", "Jhon", 20);
		DagligFast df1 = new DagligFast(inputStart, inputSlut);
		Laegemiddel lm1 = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025,
				"Styk");
		p1.setOrdination(df1);
		df1.setLaegemiddel(lm1);

		assertEquals(0.5, Service.anbefaletDosisPrDoegn(p1, lm1),0.01);	
	}

	@Test
	public void TC8() {
		LocalDate inputStart = LocalDate.of(2015, 9, 10);
		LocalDate inputSlut = LocalDate.of(2015, 10, 10);
		Patient p1 = new Patient("180793-1010", "Jhon", 70);
		DagligFast df1 = new DagligFast(inputStart, inputSlut);
		Laegemiddel lm1 = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025,
				"Styk");
		p1.setOrdination(df1);
		df1.setLaegemiddel(lm1);

		assertEquals(1.75, Service.anbefaletDosisPrDoegn(p1, lm1),0.01);	
	}

	@Test
	public void TC9() {
		LocalDate inputStart = LocalDate.of(2015, 9, 10);
		LocalDate inputSlut = LocalDate.of(2015, 10, 10);
		Patient p1 = new Patient("180793-1010", "Jhon", 140);
		DagligFast df1 = new DagligFast(inputStart, inputSlut);
		Laegemiddel lm1 = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025,
				"Styk");
		p1.setOrdination(df1);
		df1.setLaegemiddel(lm1);

		assertEquals(3.5, Service.anbefaletDosisPrDoegn(p1, lm1),0.01);	
	}

	@Test
	public void TC10() {
		LocalDate inputStart = LocalDate.of(2015, 9, 10);
		LocalDate inputSlut = LocalDate.of(2015, 10, 10);
		Patient p1 = new Patient("180793-1010", "Jhon", 24);
		DagligFast df1 = new DagligFast(inputStart, inputSlut);
		Laegemiddel lm1 = new Laegemiddel("Methotrexat", 0.01, 0.015,
				0.02, "Styk");
		p1.setOrdination(df1);
		df1.setLaegemiddel(lm1);

		assertEquals(0.24, Service.anbefaletDosisPrDoegn(p1, lm1),0.01);	
	}

	@Test
	public void TC11() {
		LocalDate inputStart = LocalDate.of(2015, 9, 10);
		LocalDate inputSlut = LocalDate.of(2015, 10, 10);
		Patient p1 = new Patient("180793-1010", "Jhon", 25);
		DagligFast df1 = new DagligFast(inputStart, inputSlut);
		Laegemiddel lm1 = new Laegemiddel("Methotrexat", 0.01, 0.015,
				0.02, "Styk");
		p1.setOrdination(df1);
		df1.setLaegemiddel(lm1);

		assertEquals(0.375, Service.anbefaletDosisPrDoegn(p1, lm1),0.001);	
	}

	@Test
	public void TC12() {
		LocalDate inputStart = LocalDate.of(2015, 9, 10);
		LocalDate inputSlut = LocalDate.of(2015, 10, 10);
		Patient p1 = new Patient("180793-1010", "Jhon", 120);
		DagligFast df1 = new DagligFast(inputStart, inputSlut);
		Laegemiddel lm1 = new Laegemiddel("Methotrexat", 0.01, 0.015,
				0.02, "Styk");
		p1.setOrdination(df1);
		df1.setLaegemiddel(lm1);

		assertEquals(1.8, Service.anbefaletDosisPrDoegn(p1, lm1),0.01);	
	}

	@Test
	public void TC13() {
		LocalDate inputStart = LocalDate.of(2015, 9, 10);
		LocalDate inputSlut = LocalDate.of(2015, 10, 10);
		Patient p1 = new Patient("180793-1010", "Jhon", 121);
		DagligFast df1 = new DagligFast(inputStart, inputSlut);
		Laegemiddel lm1 = new Laegemiddel("Methotrexat", 0.01, 0.015,
				0.02, "Styk");
		p1.setOrdination(df1);
		df1.setLaegemiddel(lm1);

		assertEquals(2.42, Service.anbefaletDosisPrDoegn(p1, lm1),0.01);	
	}

	@Test
	public void TC14() {
		Patient patient = new Patient("101910-1234", "Jan", 85);
		Laegemiddel laegemiddel = new Laegemiddel("Paracetamol", 1, 1.5, 2,
				"Ml");

		Service.opretPNOrdination(LocalDate.of(2015, 9, 9),
				LocalDate.of(2015, 10, 9), patient, laegemiddel, 123);
	}

	@Test(expected = IllegalArgumentException.class)
	public void TC15() {
		Patient patient = new Patient("101910-1234", "Jan", 85);
		Laegemiddel laegemiddel = new Laegemiddel("Paracetamol", 1, 1.5, 2,
				"Ml");

		Service.opretPNOrdination(LocalDate.of(2015, 10, 9),
				LocalDate.of(2015, 9, 9), patient, laegemiddel, 123);
	}

	@Test
	public void TC16() {
		Patient patient = new Patient("101910-1234", "Jan", 85);
		Laegemiddel laegemiddel = new Laegemiddel("Paracetamol", 1, 1.5, 2,
				"Ml");

		Service.opretPNOrdination(LocalDate.of(2015, 9, 9),
				LocalDate.of(2015, 9, 9), patient, laegemiddel, 123);
	}

	@Test
	public void TC17() {
		PN ordination = new PN(LocalDate.of(2015, 9, 9), LocalDate.of(2015, 10,
				9), 123);
		LocalDate givesDato = LocalDate.of(2015, 10, 11);
		Service.ordinationPNAnvendt(ordination, givesDato);

		assertFalse(ordination.getOrdinationer().contains(givesDato));
	}

	@Test
	public void TC18() {
		PN ordination = new PN(LocalDate.of(2015, 9, 9), LocalDate.of(2015, 10,
				9), 123);
		LocalDate givesDato = LocalDate.of(2015, 9, 8);
		Service.ordinationPNAnvendt(ordination, givesDato);

		assertFalse(ordination.getOrdinationer().contains(givesDato));
	}

	@Test
	public void TC19() {
		PN ordination = new PN(LocalDate.of(2015, 9, 9), LocalDate.of(2015, 10,
				9), 123);
		LocalDate givesDato = LocalDate.of(2015, 9, 26);
		Service.ordinationPNAnvendt(ordination, givesDato);

		assertTrue(ordination.getOrdinationer().contains(givesDato));
	}

	@Test
	public void TC20() {
		PN ordination = new PN(LocalDate.of(2015, 9, 9), LocalDate.of(2015, 10,
				9), 123);
		LocalDate givesDato = LocalDate.of(2015, 9, 9);
		Service.ordinationPNAnvendt(ordination, givesDato);

		assertTrue(ordination.getOrdinationer().contains(givesDato));
	}

	@Test
	public void TC21() {
		PN ordination = new PN(LocalDate.of(2015, 9, 9), LocalDate.of(2015, 10,
				9), 123);
		LocalDate givesDato = LocalDate.of(2015, 10, 9);
		Service.ordinationPNAnvendt(ordination, givesDato);

		assertTrue(ordination.getOrdinationer().contains(givesDato));
	}

	@Test
	public void TC22() {
		LocalDate inputStart = LocalDate.of(2015, 9, 9);
		LocalDate inputSlut = LocalDate.of(2015, 10, 9);
		Patient p1 = new Patient("180793-1010", "Jhon", 121);
		Laegemiddel lm1 = new Laegemiddel("Methotrexat", 0.01, 0.015,
				0.02, "Styk");
		Service.opretDagligFastOrdination(inputStart, inputSlut, p1, lm1, 0, 0, 0, 0);	
	}

	@Test(expected = IllegalArgumentException.class)
	public void TC23() {
		LocalDate inputStart = LocalDate.of(2015, 9, 9);
		LocalDate inputSlut = LocalDate.of(2015, 8, 9);
		Patient p1 = new Patient("180793-1010", "Jhon", 121);
		Laegemiddel lm1 = new Laegemiddel("Methotrexat", 0.01, 0.015,
				0.02, "Styk");

		Service.opretDagligFastOrdination(inputStart, inputSlut, p1, lm1, 0, 0, 0, 0);			
	}

	@Test
	public void TC24() {
		LocalDate inputStart = LocalDate.of(2015, 9, 9);
		LocalDate inputSlut = LocalDate.of(2015, 9, 9);
		Patient p1 = new Patient("180793-1010", "Jhon", 121);
		Laegemiddel lm1 = new Laegemiddel("Methotrexat", 0.01, 0.015,
				0.02, "Styk");
		Service.opretDagligFastOrdination(inputStart, inputSlut, p1, lm1, 0, 0, 0, 0);
	}

	@Test
	public void TC25() {
		Service.createSomeObjects();
		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40),
				LocalTime.of(16, 0), LocalTime.of(18, 45) };
		double[] an = { 0.5, 1, 2.5, 3 };
		Service.opretDagligSkaevOrdination(LocalDate.of(2015, 9, 9),
				LocalDate.of(2015, 10, 9), Storage.getAllPatienter().get(1),
				Storage.getAllLaegemidler().get(2), kl, an);

	}

	@Test
	public void TC26() {
		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40),
				LocalTime.of(16, 0), LocalTime.of(18, 45) };
		double[] an = { 0.5, 1, 2.5, 3 };
		DagligSkaev tc26 = Service.opretDagligSkaevOrdination(LocalDate.of(2015, 9, 9),
				LocalDate.of(2015, 8, 9), Storage.getAllPatienter().get(1),
				Storage.getAllLaegemidler().get(2), kl, an);

	}

	@Test
	public void TC27() {
		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40),
				LocalTime.of(16, 0), LocalTime.of(18, 45) };
		double[] an = { 0.5, 1, 2.5, 3 };
		Service.opretDagligSkaevOrdination(LocalDate.of(2015, 9, 9),
				LocalDate.of(2015, 9, 9), Storage.getAllPatienter().get(1),
				Storage.getAllLaegemidler().get(2), kl, an);
	}

	@Test
	public void TC28() {
		double vægtStart = 60;
		double vægtSlut = 90;
		Laegemiddel laegemiddel = Storage.getAllLaegemidler().get(0);

		assertEquals(1, Service.antalOrdinationerPrVægtPrLægemiddel(vægtStart, vægtSlut, laegemiddel), 0.1);
	}

	@Test
	public void TC29() {
		double vægtStart = 65;
		double vægtSlut = 90;
		Laegemiddel laegemiddel = Storage.getAllLaegemidler().get(0);

		assertEquals(0, Service.antalOrdinationerPrVægtPrLægemiddel(vægtStart, vægtSlut, laegemiddel), 0.1);
	}

	@Test
	public void TC30() {
		double vægtStart = 60;
		double vægtSlut = 90;
		Laegemiddel laegemiddel = Storage.getAllLaegemidler().get(1);

		assertEquals(3, Service.antalOrdinationerPrVægtPrLægemiddel(vægtStart, vægtSlut, laegemiddel), 0.1);
	}

	@Test
	public void TC34() {
		LocalDate startDen = LocalDate.of(2015, 9, 9);
		LocalDate slutDen = LocalDate.of(2015, 9, 9);
		Patient p1 = new Patient("180793-1010", "Jhon", 121);
		Laegemiddel laegemiddel = Storage.getAllLaegemidler().get(0);
		DagligFast df1 = Service.opretDagligFastOrdination(startDen, slutDen, p1, laegemiddel, 4, 2, 4, 1);

		assertEquals(11, df1.doegnDosis(),0.1);
	}

	@Test
	public void TC35() {
		LocalDate startDen = LocalDate.of(2015, 9, 9);
		LocalDate slutDen = LocalDate.of(2015, 9, 9);
		Patient p1 = new Patient("180793-1010", "Jhon", 121);
		Laegemiddel laegemiddel = Storage.getAllLaegemidler().get(0);
		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40),
				LocalTime.of(16, 0), LocalTime.of(18, 45) };
		double[] antal = {2,2,2,2};
		DagligSkaev df1 = Service.opretDagligSkaevOrdination(startDen, slutDen, p1, laegemiddel, kl, antal);

		assertEquals(8, df1.doegnDosis(),0.1);
	}

	//	@Test
	//	public void TC36() {
	//		LocalDate startDen = LocalDate.of(2015, 9, 9);
	//		LocalDate slutDen = LocalDate.of(2015, 9, 9);
	//		Patient p1 = new Patient("180793-1010", "Jhon", 121);
	//		Laegemiddel laegemiddel = Storage.getAllLaegemidler().get(0);
	//		PN pn1 = Service.opretPNOrdination(startDen, slutDen, p1, laegemiddel, 2);
	//		
	//		assertEquals(8, pn1.doegnDosis(),0.1);
	//	}

	@Test
	public void TC37() {
		Patient patient = new Patient("101910-1234", "Jan", 85);
		Laegemiddel laegemiddel = new Laegemiddel("Paracetamol", 1, 1.5, 2,
				"Ml");

		PN pn = Service.opretPNOrdination(LocalDate.of(2015, 9, 9),
				LocalDate.of(2015, 9, 15), patient, laegemiddel, 123);

		assertEquals(7, pn.antalDage(), 1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void TC38() {
		Patient patient = new Patient("101910-1234", "Jan", 85);
		Laegemiddel laegemiddel = new Laegemiddel("Paracetamol", 1, 1.5, 2,
				"Ml");

		PN pn = Service.opretPNOrdination(LocalDate.of(2015, 9, 9),
				LocalDate.of(2015, 9, 7), patient, laegemiddel, 123);
	}

	@Test
	public void TC39() {
		Patient patient = new Patient("101910-1234", "Jan", 85);
		Laegemiddel laegemiddel = new Laegemiddel("Paracetamol", 1, 1.5, 2,
				"Ml");

		PN pn = Service.opretPNOrdination(LocalDate.of(2015, 9, 9),
				LocalDate.of(2015, 10, 10), patient, laegemiddel, 123);

		assertEquals(31, pn.antalDage(), 1);
	}

	@Test
	public void TC40() {

		Patient patient = new Patient("101910-1234", "Jan", 85);
		Laegemiddel laegemiddel = new Laegemiddel("Paracetamol", 1, 1.5, 2,
				"Ml");
		DagligFast df = Service.opretDagligFastOrdination(
				LocalDate.of(2015, 9, 9), LocalDate.of(2015, 9, 18), patient,
				laegemiddel, 2, 2, 2, 4);

		assertEquals(100, df.samletDosis(), 1);
	}

	@Test
	public void TC41() {

		Patient patient = new Patient("101910-1234", "Jan", 85);
		Laegemiddel laegemiddel = new Laegemiddel("Paracetamol", 1, 1.5, 2,
				"Ml");
		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40),
				LocalTime.of(16, 0)};
		double[] antal = {2,2,1};
		DagligSkaev ds = Service.opretDagligSkaevOrdination(
				LocalDate.of(2015, 9, 9), LocalDate.of(2015, 9, 20), patient,
				laegemiddel, kl, antal);
		assertEquals(60, ds.samletDosis(), 1);
	}

	@Test
	public void TC42() {
		Patient patient = new Patient("101910-1234", "Jan", 85);
		Laegemiddel laegemiddel = new Laegemiddel("Paracetamol", 1, 1.5, 2,
				"Ml");

		PN pn = Service.opretPNOrdination(LocalDate.of(2015, 9, 9),
				LocalDate.of(2015, 10, 10), patient, laegemiddel, 5);

		pn.givDosis(LocalDate.of(2015, 9, 9));
		pn.givDosis(LocalDate.of(2015, 9, 10));
		pn.givDosis(LocalDate.of(2015, 9, 11));
		pn.givDosis(LocalDate.of(2015, 9, 12));

		assertEquals(20, pn.samletDosis(), 1);
	}
}