package ordination;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {

	private Dosis[] dosiser = new Dosis[4];

	public DagligFast(LocalDate inputStart, LocalDate inputSlut) {
		super(inputStart, inputSlut);
		dosiser[0] = new Dosis(LocalTime.of(8, 00), 0);
		dosiser[1] = new Dosis(LocalTime.of(12, 00), 0);
		dosiser[2] = new Dosis(LocalTime.of(18, 00), 0);
		dosiser[3] = new Dosis(LocalTime.of(00, 00), 0);
	}

	public void setDosisMorgen(double antal) {
		dosiser[0].setAntal(antal);
	}

	public void setDosisMiddag(double antal) {
		dosiser[1].setAntal(antal);
	}

	public void setDosisAften(double antal) {
		dosiser[2].setAntal(antal);
	}

	public void setDosisNat(double antal) {
		dosiser[3].setAntal(antal);
	}

	@Override
	public double samletDosis() {
		double resualt = 0;
		for (Dosis dosis : dosiser) {
			resualt += dosis.getAntal();
		}
		return resualt * super.antalDage();
	}

	@Override
	public double doegnDosis() {
		double resualt = 0;
		for (Dosis dosis : dosiser) {
			resualt += dosis.getAntal();
		}
		return resualt;
	}

	@Override
	public String getType() {
		return super.getLaegemiddel().getEnhed();
	}

	public Dosis[] getDoser() {
		return this.dosiser;
	}
}
