package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
    // TODO
	ArrayList<Dosis> dosiser = new ArrayList<>();
	
	public DagligSkaev(LocalDate startDate, LocalDate slutDate) {
		super(startDate, slutDate);
	}

	
    public void opretDosis(LocalTime tid, double antal) {
        // TODO
    	dosiser.add(new Dosis(tid, antal));
    	
    	
    }


	@Override
	public double samletDosis() {
		double sum = 0;
		for (Dosis dosis : dosiser) {
			sum += dosis.getAntal() * super.antalDage();
		}
		return sum;
	}


	@Override
	public double doegnDosis() {
		double sum = 0;
		for (Dosis dosis : dosiser) {
			sum += dosis.getAntal();
		}
		return sum;
	}


	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return super.getLaegemiddel().getEnhed();
	}
	public ArrayList<Dosis> getDoser() {
		return dosiser;
	}
}
