package ordination;

import java.time.LocalDate;
import java.util.ArrayList;

public class PN extends Ordination {

	private double antalEnheder;
	private ArrayList<LocalDate> ordinationer = new ArrayList<>();

	public PN(LocalDate inputStart, LocalDate inputSlut,
			double inputAntalEnheder) {
		super(inputStart, inputSlut);
		this.antalEnheder = inputAntalEnheder;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true
	 * hvis givesDen er inden for ordinationens gyldighedsperiode og datoen
	 * huskes Retrurner false ellers og datoen givesDen ignoreres
	 *
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		if (givesDen.compareTo(getSlutDen()) <= 0
				&& givesDen.compareTo(getStartDen()) >= 0) {
			ordinationer.add(givesDen);
			return true;
		}
		return false;
	}

	@Override
	public double doegnDosis() {
		return (getAntalGangeGivet() * antalEnheder) / super.antalDage();
	}

	@Override
	public double samletDosis() {
		return getAntalGangeGivet() * antalEnheder;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 *
	 * @return
	 */
	public int getAntalGangeGivet() {
		return ordinationer.size();
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}
	
	
	public ArrayList<LocalDate> getOrdinationer() {
		return ordinationer;
	}

	@Override
	public String getType() {
		return super.getLaegemiddel().getEnhed();
	}
}
